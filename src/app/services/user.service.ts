import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {User} from '../models/user';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  numEmployees = 3;
  total = 2;
  loading = true;
  employees: User[] = [];
  employeesPaginate: User[] = [];
  randomUserUrl = 'https://api.randomuser.me/'; // TODO: temp api
  pageIndex = 1;
  pageSize = 10;

  constructor(private http: HttpClient) {
    this.addEmployee({
      id: 1,
      username: 'superUser',
      password: 'superUser',
      email: 'super@email.com',
      firstName: 'super',
      lastNamePrefix: null,
      lastName: 'user',
      avatar: 'avatar.png'
    });
    this.addEmployee({
      id: 2,
      username: 'werknemer',
      password: 'werknemer',
      email: 'emplo@yee.com',
      firstName: 'emplo',
      lastNamePrefix: null,
      lastName: 'yee',
      avatar: 'avatar.png'
    });
    this.addEmployee({
      email: 'new@employee.com',
      firstName: 'new',
      lastNamePrefix: null,
      lastName: 'user',
    });

    const params = new HttpParams()
      .append('results', '20')
      .append('nat', 'nl')
      .append('seed', 'ewa')
      .append('inc', 'email,name,picture,login,id');
    this.http.get(`${this.randomUserUrl}`, {params})
      .subscribe((data: any) => {
        data.results.forEach(employee => {
          const {id, login, name, picture} = employee;
          this.addEmployee({
              id: +id.value,
              username: login.username,
              password: login.password,
              email: employee.email,
              firstName: name.first,
              lastNamePrefix: null,
              lastName: name.last,
              avatar: picture.large
            }
          );
        });

        this.paginate();
        this.loading = false;
      });
  }

  public paginate(): void {
    this.loading = true;
    const employees = [].concat(this.employees);
    this.employeesPaginate = employees.slice((this.pageIndex - 1) * this.pageSize, this.pageIndex * this.pageSize);
    this.loading = false;
  }

  public addEmployee(data: any): number {
    this.total++;
    this.numEmployees++;
    return this.employees
      .push(new User(
        data.email,
        data.firstName,
        data.lastNamePrefix,
        data.lastName,
        {
          id: (data.id === undefined) ? this.numEmployees : +data.id,
          username: data.username,
          password: data.password,
          avatar: data.avatar
        })) - 1;
  }

  public removeEmployee(id: number) {
    this.total--;
    const idToDelete = this.employees.findIndex(employee => {
      return employee.id === id;
    });

    return this.employees.splice(idToDelete, 1)[0];
  }

  isLoggedIn(): User | null {
    const id = Number(localStorage.getItem('user_id'));
    return this.employees.find(employee => {
      return employee.id === id;
    });
  }

  login(username: string, password: string) {
    const user = this.employees.find(employee => {
      return (employee.username || employee.email) === username && employee.password === password;
    });

    if (user) {
      localStorage.setItem('user_id', user.id.toString());
      localStorage.setItem('admin', user.username === 'superUser' ? '1' : '0');
      localStorage.setItem('user', JSON.stringify(user));
      return user;
    } else {
      return false;
    }
  }

  logout() {
    localStorage.clear();
  }
}
