import {Injectable} from '@angular/core';
import {Question} from '../models/question';
import {Subject} from '../models/subject';

@Injectable({
  providedIn: 'root'
})
export class QuestionaireService {
  private questions: Question[];
  private nature = new Subject('Natuur beleven', 'https://images.unsplash.com/photo-1470116073782-48ae2ccd8ffd', 'test info natuur beleven');
  private gardening = new Subject('Tuinieren', 'https://www.almanac.com/sites/default/files/image_nodes/landscaping_for_low_maintenance2-thinkstockphotos.jpg', 'test info tuinieren');
  private restandrelax = new Subject('Rust & Ontspanning', 'https://www.inspirerendleven.nl/wp-content/uploads/2016/08/shutterstock_373900177-e1485274393849.jpg', 'test info rust & ontspanning');
  private meeting = new Subject('Ontmoeting', 'https://s24193.pcdn.co/wp-content/uploads/2017/05/apps-for-making-friends-entity-1320x720.jpg', 'test info ontmoeten');
  private moving = new Subject('Bewegen', 'https://www.sljeme.run/wp-content/uploads/2015/12/iStock_000014325735Medium-994x610.jpg', 'test info bewegen');

  constructor() {
    this.questions = [
      new Question(this.nature, this.gardening),
      new Question(this.restandrelax, this.meeting),
      new Question(this.nature, this.moving),
      new Question(this.gardening, this.restandrelax),
      new Question(this.meeting, this.moving),
      new Question(this.gardening, this.meeting),
      new Question(this.restandrelax, this.nature),
      new Question(this.moving, this.gardening),
      new Question(this.nature, this.meeting),
      new Question(this.moving, this.restandrelax)
    ];
  }


  /**
   * Get array of questions.
   *
   * @return Question[]
   */
  public getQuestions(): Question[] {
    return this.questions;
  }

  /**
   * Get question by index.
   *
   * @param questionIndex number
   *
   * @return Question
   */
  public getQuestionByIndex(questionIndex: number): Question {
    return this.questions[questionIndex];
  }

  /**
   * Get number of questions.
   *
   * @return number
   */
  public getNumberOfQuestions(): number {
    return this.questions.length;
  }
}
