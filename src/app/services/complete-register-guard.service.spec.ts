import { TestBed } from '@angular/core/testing';

import { CompleteRegisterGuardService } from './complete-register-guard.service';

describe('CompleteRegisterGuardService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CompleteRegisterGuardService = TestBed.get(CompleteRegisterGuardService);
    expect(service).toBeTruthy();
  });
});
