import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class AmsterdamApiService {
  apiURL = 'https://api.data.amsterdam.nl/';

  constructor(private httpClient: HttpClient) {
  }

  public getAreas() {
    return this.httpClient.get(`${this.apiURL}/gebieden/stadsdeel/`);
  }

  public getArea(id: number) {
    return this.httpClient.get(`${this.apiURL}/gebieden/stadsdeel/${id}`);
  }

  public getNeighbourhoods(inArea: number | false) {
    return this.httpClient.get(`${this.apiURL}/gebieden/buurtcombinatie/${inArea ? '?stadsdeel=' + inArea : ''}`);
  }

  public getNeighbourhood(id: number) {
    return this.httpClient.get(`${this.apiURL}/gebieden/buurtcombinatie/${id}`);
  }

  public getDistricts(inNeighbourhood: number | false) {
    return this.httpClient.get(`${this.apiURL}/gebieden/buurt/${inNeighbourhood ? '?buurtcombinatie=' + inNeighbourhood : ''}`);
  }

  public getDistrict(id: number) {
    return this.httpClient.get(`${this.apiURL}/gebieden/buurt/${id}`);
  }
}
