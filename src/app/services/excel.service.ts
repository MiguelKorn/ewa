import { Injectable } from '@angular/core';
import * as FileSaver from 'file-saver';
import * as XLSX from 'xlsx';

const EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
const EXCEL_EXTENSION = '.xlsx';

@Injectable({
  providedIn: 'root'
})
export class ExcelService {

  constructor() { }

  export(json: any[], filename: string) {
    const ws: XLSX.WorkSheet = XLSX.utils.json_to_sheet(json);
    const wb: XLSX.WorkBook = {Sheets: {data: ws}, SheetNames: ['data']};
    const fileBuffer: any = XLSX.write(wb, {bookType: 'xlsx', type: 'array'});
    this.save(fileBuffer, filename);
  }

  private save(buffer: any, filename: string) {
    const data: Blob = new Blob([buffer], {
      type: EXCEL_TYPE
    });
    const today = new Date();
    const date = today.getFullYear() + '' + (today.getMonth() + 1) + '' + today.getDate() + '_';
    const time = today.getHours() + '-' + today.getMinutes() + '-' + today.getSeconds();
    const name = filename + date + time;
    FileSaver.saveAs(data, name + EXCEL_EXTENSION);
  }
}
