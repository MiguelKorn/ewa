import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivateChild, Router, RouterStateSnapshot} from '@angular/router';
import {UserService} from './user.service';

@Injectable({
  providedIn: 'root'
})
export class CompleteRegisterGuardService implements CanActivateChild {
  constructor(private userService: UserService, private router: Router) {
  }

  canActivateChild(childRoute: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    const user = this.userService.isLoggedIn();
    const editPwdRoute = '/admin/account/edit-pwd';
    const editRoute = '/admin/account/edit';

    if ((state.url === ((user.resetPassword === true ? editPwdRoute : editRoute))) ||
      (user && user.resetPassword === false && user.fillCredentials === false)) {
      return true;
    }

    this.router.navigate([(user.resetPassword === true ? editPwdRoute : editRoute)]);
    return false;
  }
}
