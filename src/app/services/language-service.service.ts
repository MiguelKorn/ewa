import {EventEmitter, Injectable} from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class LanguageServiceService {

  private _taal: string = "Nederlands";
  onLanguageChange: EventEmitter<string> = new EventEmitter<string>();

  constructor() { }

  get getTaal(): string {
    return this._taal;
  }

  set setTaal(value: string) {
    this._taal = value;
    this.onLanguageChange.emit(this._taal);
  }

}
