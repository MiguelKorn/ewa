import {Injectable} from '@angular/core';
import {Project} from '../models/project';

@Injectable({
  providedIn: 'root'
})
export class ProjectService {
  projects: Project[];


  constructor() {
    this.projects = [];
    this.projects.push(new Project('0', 'HvA plantsoen', 'nieuw plantsoen bij de locatie van de hva', null, null, null, {
      latitude: 52.34585392032272,
      longitude: 4.864143858200123
    }, 'plantsoen', null, new Date('07-09-2020')));
    this.projects.push(new Project('1', 'Rembrandtplein plantsoen', 'nieuw plantsoen op het rembrandtplein', null, null, null, {
      latitude: 52.34585392032272,
      longitude: 4.864143858200123
    }, 'plantsoen', null, new Date('07-09-2020')));
    this.projects.push(new Project('2', 'Oosterpark', 'nieuw bestemmingsplan Oosterpark', null, null, null, {
      latitude: 52.34585392032272,
      longitude: 4.864143858200123
    }, 'plantsoen', null, new Date('07-09-2020')));
  }

  add(project: Project) {
    if (project) {
      this.projects.push(project);
    }
  }

  remove(id: string) {
    const idToDelete = this.projects.findIndex(project => {
      return project.id === id;
    });

    return this.projects.splice(idToDelete, 1)[0];
  }

  update() {
    //even kijken of we het op index of op zoeken gaan doen
  }
}
