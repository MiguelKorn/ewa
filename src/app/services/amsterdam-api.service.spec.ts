import { TestBed } from '@angular/core/testing';

import { AmsterdamApiService } from './amsterdam-api.service';

describe('AmsterdamApiService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AmsterdamApiService = TestBed.get(AmsterdamApiService);
    expect(service).toBeTruthy();
  });
});
