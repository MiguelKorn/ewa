import {Injectable} from '@angular/core';
import {Result} from '../models/result';
import {QuestionaireService} from './questionaire.service';
import {checkVersion} from '@angular/compiler-cli/src/transformers/program';
import {validateComponent} from 'codelyzer/walkerFactory/walkerFn';

@Injectable({
  providedIn: 'root'
})
export class ResultService {
  results: Result[] = [];

  constructor() {
    for (let i = 0; i < 10; i++) {
      this.addRandomResult();
    }
  }

  addRandomResult() {
    const age = Math.floor(Math.random() * (99 - 18 + 1)) + 18;
    const gender = Math.round(Math.random()) === 1 ? 'm' : 'v';
    const zipcode = 1000 + Math.floor(Math.random() * 99);
    const email = 'gebruiker' + Math.floor(100000 + Math.random() * 999999) + '@example.com';
    const projectId = Math.floor(Math.random() * 3);
    const questions = new QuestionaireService().getQuestions().map(question => {
      question.answer = Math.floor(Math.random() * 5) + 1;
      return question;
    });

    this.results.push(
      new Result(
        this.results.length + 1,
        Math.round(Math.random()) === 1 ? age : null,
        Math.round(Math.random()) === 1 ? gender : null,
        Math.round(Math.random()) === 1 ? zipcode : null,
        Math.round(Math.random()) === 1 ? email : null,
        projectId,
        null,
        questions
      )
    );
  }
}
