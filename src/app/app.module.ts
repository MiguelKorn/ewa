import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {SignInPageComponent} from './components/admin/sign-in-page/sign-in-page.component';
import {MainComponent} from './components/questionaire/main/main.component';
import {MainComponent as AdminMainComponent} from './components/admin/main/main.component';
import {NotfoundComponent} from './components/error/notfound/notfound.component';
import {RouterModule} from '@angular/router';
import {NgZorroAntdModule, NZ_I18N, nl_NL} from 'ng-zorro-antd';
import {HttpClientModule} from '@angular/common/http';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {registerLocaleData} from '@angular/common';
import nl from '@angular/common/locales/nl';
import {AuthGuardService} from './services/auth-guard.service';
import {QuestionaireComponent} from './components/questionaire/questionaire/questionaire.component';
import {RoadmapComponent} from './components/questionaire/questionaire/roadmap/roadmap.component';
import {QuestionaireService} from './services/questionaire.service';
import {ResultComponent} from './components/questionaire/result/result.component';
import {EmotionbarComponent} from './components/questionaire/questionaire/emotionbar/emotionbar.component';
import {EmployeeListComponent} from './components/admin/employee/list/employee-list.component';
import {HomeComponent} from './components/admin/home/home.component';
import { EmployeeDetailComponent } from './components/admin/employee/detail/employee-detail.component';
import { EmployeeCreateComponent } from './components/admin/employee/create/employee-create.component';
import { ProjectCreateComponent } from './components/admin/project/create/project-create.component';
import { ProjectListComponent } from './components/admin/project/list/project-list.component';
import { CodeComponent } from './components/questionaire/code/code.component';
import { IntroComponent } from './components/questionaire/intro/intro.component';
import { GeneralinfoComponent } from './components/questionaire/generalinfo/generalinfo.component';
import { AgmCoreModule } from '@agm/core';
import { ProjectEditComponent } from './components/admin/project/edit/project-edit.component';
import {EmployeeEditComponent} from './components/admin/employee/edit/employee-edit.component';
import {EmployeeEditPwdComponent} from './components/admin/employee/edit-pwd/employee-edit-pwd.component';
import {CompleteRegisterGuardService} from './services/complete-register-guard.service';
import { ProjectListAllComponent } from './components/admin/project/list-all/project-list-all.component';
import { OutroComponent } from './components/questionaire/outro/outro.component';

registerLocaleData(nl);

const appRoutes = [
  {path: '', component: MainComponent,
  children:[
    {path: '', component: GeneralinfoComponent},
    {path: 'intro', component: IntroComponent},
  ]},
  {
    path: 'admin',
    component: AdminMainComponent,
    canActivate: [AuthGuardService],
    canActivateChild: [CompleteRegisterGuardService],
    children: [
      {path: '', redirectTo: 'home', pathMatch: 'full'},
      {path: 'home', component: HomeComponent},
      {path: 'account/edit-pwd', component: EmployeeEditPwdComponent},
      {path: 'account/edit', component: EmployeeEditComponent},
      {path: 'account', component: EmployeeDetailComponent},
      {path: 'employees/add', component: EmployeeCreateComponent},
      {path: 'employees/:id', component: EmployeeDetailComponent},
      {path: 'employees', component: EmployeeListComponent},
      {path: 'projects/add', component: ProjectCreateComponent},
      {path: 'projects/list', component: ProjectListComponent},
      {path: 'projects/edit', component: ProjectEditComponent},
      {path: 'projects/list-all', component: ProjectListAllComponent},
      {path: '**', redirectTo: 'home'}
    ]
  },
  {path: 'login', component: SignInPageComponent},
  {path: 'questionaire', component: QuestionaireComponent},
  {path: 'outro', component: OutroComponent},
  {path: 'result', component: ResultComponent},
  {path: '**', component: NotfoundComponent}
];

@NgModule({
  declarations: [
    AppComponent,
    SignInPageComponent,
    MainComponent,
    AdminMainComponent,
    NotfoundComponent,
    QuestionaireComponent,
    RoadmapComponent,
    ResultComponent,
    EmotionbarComponent,
    EmployeeListComponent,
    EmployeeDetailComponent,
    EmployeeCreateComponent,
    HomeComponent,
    ProjectCreateComponent,
    ProjectListComponent,
    CodeComponent,
    IntroComponent,
    GeneralinfoComponent,
    ProjectEditComponent,
    EmployeeEditComponent,
    EmployeeEditPwdComponent,
    ProjectListAllComponent,
    OutroComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    RouterModule.forRoot(appRoutes),
    NgZorroAntdModule,
    HttpClientModule,
    BrowserAnimationsModule,
    ReactiveFormsModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyAoqIcqzFxq7vw2IuGcW3NOGXFoZEE-N3M'
    })
  ],
  providers: [
    {provide: NZ_I18N, useValue: nl_NL},
    QuestionaireService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
