import { Component, OnInit } from '@angular/core';
import { ProjectService } from 'src/app/services/project.service';
import { Project } from 'src/app/models/project';
import { ActivatedRoute, Router } from '@angular/router';
import { LatLngLiteral } from '@agm/core';

@Component({
  selector: 'app-project-edit',
  templateUrl: './project-edit.component.html',
  styleUrls: ['./project-edit.component.css']
})
export class ProjectEditComponent implements OnInit {

  constructor(private projectService : ProjectService, private route : ActivatedRoute, private router : Router) { }
  project : Project;
  projectIndex : number;
  startLat : number;
  startLng : number;
  ngOnInit() {
    this.route.queryParamMap.subscribe(params =>{
      console.log(params);
      this.projectIndex = parseInt(params.get("projectIndex"));
    });
    this.project = this.projectService.projects[this.projectIndex];
    console.log(this.project);
    console.log(this.projectIndex);
    this.startLat = 52.379189;
    this.startLng = 4.899431;
  }

  changePickupMarkerLocation($event: { coords:LatLngLiteral}) {
    this.project.position.latitude =$event.coords.lat;
    this.project.position.longitude =$event.coords.lng;
    }

  saveProject(){
    this.projectService.projects[this.projectIndex] = this.project;
    this.router.navigate(["admin/projects/list"]);

  }



}
