import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ProjectService } from 'src/app/services/project.service';
import { Project } from 'src/app/models/project';
import {ActivatedRoute, Router} from '@angular/router';
import { LatLngLiteral } from '@agm/core';

@Component({
  selector: 'app-project-create',
  templateUrl: './project-create.component.html',
  styleUrls: ['./project-create.component.css']
})
export class ProjectCreateComponent implements OnInit {
  return : string;
  newProject : Project;
  constructor(private projectService: ProjectService, private router: Router, private route: ActivatedRoute) { }
  startLat : number;
  startLng : number;
  markerLat : number;
  markerLng : number;
  showMarker: boolean;
  ngOnInit() {
    this.route.queryParams.subscribe(params => this.return = params.return || 'admin/projects/list');
    this.startLat = 52.379189;
    this.startLng = 4.899431;
    this.showMarker = false;
  }

  changePickupMarkerLocation($event: { coords:LatLngLiteral}) {
    this.markerLat=$event.coords.lat;
    this.markerLng=$event.coords.lng;
    this.showMarker=true;
    }

  addProject(form : NgForm){
    console.log(this.projectService.projects);
    console.log(form);
    if(form.valid){
      this.newProject = new Project(form.value.projectId, 
        form.value.projectName,
        form.value.projectDescription,
        null, 
        null,
        null,
        {latitude : this.markerLat,longitude: this.markerLng},
        form.value.projectType,
        null, 
        form.value.projectEnd);
      this.projectService.add(this.newProject);
      this.router.navigateByUrl(this.return);

    } else{
      console.log("form not valid");
    }
  }

}
