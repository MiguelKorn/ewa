import { Component, OnInit } from '@angular/core';
import { ProjectService } from 'src/app/services/project.service';
import { Project } from 'src/app/models/project';
import { Router } from '@angular/router';

@Component({
  selector: 'app-project-list',
  templateUrl: './project-list.component.html',
  styleUrls: ['./project-list.component.css']
})
export class ProjectListComponent implements OnInit {

  projects: Project[];
  projectIndex : number;
  constructor(private projectService : ProjectService, private router : Router) { }

  ngOnInit() {
    this.projects = this.projectService.projects;
    console.log(this.projects)
  }

  delete(project : Project){
    this.projectService.remove(project.id);
  }

  edit(project : Project){
    this.projectIndex = this.projectService.projects.indexOf(project);

    this.router.navigate(["admin/projects/edit"], {queryParams: {projectIndex : this.projectIndex}});
  }

}
