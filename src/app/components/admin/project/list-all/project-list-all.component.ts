import {Component, OnInit} from '@angular/core';
import {Project} from '../../../../models/project';
import {ProjectService} from '../../../../services/project.service';
import * as moment from 'moment';
import {ResultService} from '../../../../services/result.service';
import {Result} from '../../../../models/result';
import {ExcelService} from '../../../../services/excel.service';
import _ from 'lodash';

moment.locale('nl');

@Component({
  selector: 'app-project-list-all',
  templateUrl: './project-list-all.component.html',
  styleUrls: ['./project-list-all.component.css']
})

export class ProjectListAllComponent implements OnInit {
  projects: Project[];
  results: Result[];
  excelResults: any[];
  modalVisible = false;
  isConfirmLoading = false;
  showAllColumns = false;
  indeterminate = false;
  mapOfShownColumns = [
    {label: 'Leeftijd', value: 'age', checked: false},
    {label: 'Geslacht', value: 'gender', checked: false},
    {label: 'Postcode', value: 'zipcode', checked: false},
    {label: 'Email', value: 'email', checked: false},
    {label: 'Score', value: 'score', checked: false},
    {label: 'Resultaten', value: 'results', checked: false}
  ];

  constructor(private projectService: ProjectService,
              private resultService: ResultService,
              private excelService: ExcelService) {
  }

  ngOnInit() {
    this.projects = this.projectService.projects;
    this.results = this.resultService.results;
  }

  formatDate(date: Date) {
    return moment(date).format('LLLL');
  }

  parseExcelResults() {
    this.excelResults = this.results.map(r => {
      const result = _.clone(r);
      if (!this.showAllColumns || this.indeterminate) {
        this.mapOfShownColumns.forEach(({checked, value}) => {
          if (!checked) {
            if (value === 'results') {
              delete result.questions;
            } else {
              delete result[value];
            }
          }
        });
      }

      if (result.questions) {
        result.questions.forEach((q, i) => result['question_' + (i + 1)] = q.answer);
      }

      delete result.questions;
      return result;
    });
  }

  showModal() {
    this.modalVisible = true;
  }

  handleModalSubmit() {
    this.isConfirmLoading = true;
    this.parseExcelResults();
    this.excelService.export(this.excelResults, 'resultaten');

    // setTimeout(() => {
    this.isConfirmLoading = false;
    this.modalVisible = false;
    // }, 3000);
  }

  handleModalCancel() {
    this.modalVisible = false;
  }

  updateAllChecked() {
    this.indeterminate = false;
    if (this.showAllColumns) {
      this.mapOfShownColumns = this.mapOfShownColumns.map(item => ({...item, checked: true}));
    } else {
      this.mapOfShownColumns = this.mapOfShownColumns.map(item => ({...item, checked: false}));
    }
  }

  updateSingleChecked(): void {
    if (this.mapOfShownColumns.every(item => !item.checked)) {
      this.showAllColumns = false;
      this.indeterminate = false;
    } else if (this.mapOfShownColumns.every(item => item.checked)) {
      this.showAllColumns = true;
      this.indeterminate = false;
    } else {
      this.indeterminate = true;
    }
  }
}
