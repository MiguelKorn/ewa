import {Component, OnInit} from '@angular/core';
import {NgForm} from '@angular/forms';
import {User} from '../../../models/user';
import {UserService} from '../../../services/user.service';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-sign-in-page',
  templateUrl: './sign-in-page.component.html',
  styleUrls: ['./sign-in-page.component.css']
})
export class SignInPageComponent implements OnInit {

  superUser: User;
  werknemer: User;
  return: string;

  constructor(private userService: UserService, private router: Router, private route: ActivatedRoute) {
  }

  ngOnInit() {
    this.route.queryParams.subscribe(params => this.return = params.return || '/admin');
  }

  checkCred(form: NgForm) {
    if (this.userService.login(form.value.username, form.value.password)) {
      console.log('goeie login');
      console.log(this.return);
      this.router.navigateByUrl(this.return);
    } else {
      console.log('foute login');
    }
  }

}
