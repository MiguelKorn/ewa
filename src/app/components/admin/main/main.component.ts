import {Component, Directive, OnInit} from '@angular/core';
import {ActivatedRoute, NavigationEnd, NavigationStart, Router} from '@angular/router';
import {UserService} from '../../../services/user.service';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent implements OnInit {
  isCollapsed: boolean;
  openMap: { [name: string]: boolean } = {
    projects: false,
    employees: false,
  };
  user: any;

  constructor(private userService: UserService,
              private router: Router,
              private route: ActivatedRoute) {
  }

  ngOnInit() {
    this.isCollapsed = false;
    this.changeOpenDrawer(this.router.url);
    this.router.events.subscribe(value => {
      if (value instanceof NavigationEnd) {
        this.changeOpenDrawer(value.url);
      }
    });
    this.user = JSON.parse(localStorage.getItem('user'));
  }

  openHandler(value: string): void {
    for (const key in this.openMap) {
      if (key !== value) {
        this.openMap[key] = false;
      }
    }
  }

  changeOpenDrawer(value) {
    if (RegExp('(\/projects?.+)').test(value)) {
      this.openMap.projects = true;
      this.openHandler('projects');
    } else if (RegExp('(\/employees?.+)').test(value)) {
      this.openMap.employees = true;
      this.openHandler('employees');
    }
  }

  isRouterLinkActive(route: string, exact: boolean = false): boolean {
    return this.router.isActive(route, exact);
  }

  logout() {
    this.userService.logout();
    this.router.navigate(['login'], {queryParams: {loggedOut: true}});
  }
}
