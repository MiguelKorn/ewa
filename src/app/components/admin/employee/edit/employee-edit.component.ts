import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, ValidatorFn, Validators} from '@angular/forms';
import {UserService} from '../../../../services/user.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-employee-edit',
  templateUrl: './employee-edit.component.html',
  styleUrls: ['./employee-edit.component.css']
})
export class EmployeeEditComponent implements OnInit {
  form: FormGroup;
  isValidPhone = true;
  showHelp = false;

  constructor(private formBuilder: FormBuilder,
              private userService: UserService,
              private router: Router) {
  }

  ngOnInit() {
    const user = this.userService.isLoggedIn();
    if (user.fillCredentials) {
      this.showHelp = true;
    }
    console.log(user.phoneNumber);
    this.form = this.formBuilder.group({
      firstName: [user.firstName, [Validators.required]],
      lastNamePrefix: [user.lastNamePrefix],
      lastName: [user.lastName, [Validators.required]],
      email: [user.email, Validators.email],
      phoneNumberPrefix: [user.phonePrefix || 'mob'],
      phoneNumber: [user.phoneNumber || '', Validators.required],
      function: [user.function, Validators.required],
      area: [(user.area), Validators.required]
    }, {validator: this.phoneNumberValidator()});

    this.form.valueChanges.subscribe(data => {
      const {phoneNumber, phoneNumberPrefix} = data;
      this.isValidPhone = phoneNumber === ('' || null || undefined) ? true : this.isValidPhoneNumber(phoneNumber, phoneNumberPrefix)
    });
  }

  phoneNumberValidator(): ValidatorFn {
    return (group: FormGroup): { [key: string]: any } | null => {
      let {phoneNumber, phoneNumberPrefix} = group.controls;
      phoneNumber = phoneNumber.value;
      phoneNumberPrefix = phoneNumberPrefix.value;
      const isValid = this.isValidPhoneNumber(phoneNumber, phoneNumberPrefix);
      return isValid ? null : {phoneNumber: {value: phoneNumber}};
    };
  }

  isValidPhoneNumber(num, prefix): boolean {
    return num.toString().length + ((prefix === 'mob' ? 2 : 3)) === 10;
  }

  addCredentials() {
    const data = this.form.getRawValue();
    const user = this.userService.isLoggedIn();
    user.firstName = data.firstName;
    user.lastNamePrefix = data.lastNamePrefix;
    user.lastName = data.lastName;
    user.email = data.email;
    user.phonePrefix = data.phonePrefix;
    user.phoneNumber = data.phoneNumber;
    user.function = data.function;
    user.area = data.area;
    user.fillCredentials = false;
    this.router.navigate(['/admin/account']);
  }
}
