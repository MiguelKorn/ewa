import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {UserService} from '../../../../services/user.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-employee-edit-pwd',
  templateUrl: './employee-edit-pwd.component.html',
  styleUrls: ['./employee-edit-pwd.component.css']
})
export class EmployeeEditPwdComponent implements OnInit {
  form: FormGroup;
  showHelp = false;

  constructor(private formBuilder: FormBuilder,
              private userService: UserService,
              private router: Router) {
  }

  ngOnInit() {
    const user = this.userService.isLoggedIn();
    if (user.resetPassword) {
      this.showHelp = true;
    }
    this.form = this.formBuilder.group({
      password: ['', [Validators.required]],
      confirm: ['', [this.confirmationValidator]]
    });
  }

  confirmationValidator = (control: FormControl): { [s: string]: boolean } => {
    if (!control.value) {
      return { required: true };
    } else if (control.value !== this.form.controls.password.value) {
      return { confirm: true, error: true };
    }
    return {};
  }

  updateConfirmValidator() {
    Promise.resolve().then(() => this.form.controls.confirm.updateValueAndValidity());
  }

  changePassword() {
    const data = this.form.getRawValue();
    const user = this.userService.isLoggedIn();
    console.log(this.userService.employees);
    user.password = data.password;
    user.resetPassword = false;
    console.log(this.userService.employees);
    this.router.navigate(['/admin/account']);
  }
}
