import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmployeeEditPwdComponent } from './employee-edit-pwd.component';

describe('EmployeeEditPwdComponent', () => {
  let component: EmployeeEditPwdComponent;
  let fixture: ComponentFixture<EmployeeEditPwdComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmployeeEditPwdComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmployeeEditPwdComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
