import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {UserService} from '../../../../services/user.service';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-employee-create',
  templateUrl: './employee-create.component.html',
  styleUrls: ['./employee-create.component.css']
})
export class EmployeeCreateComponent implements OnInit {
  form: FormGroup;
  isValidPhone = true;


  constructor(private formBuilder: FormBuilder,
              private userService: UserService,
              private router: Router,
              private route: ActivatedRoute) {
  }

  ngOnInit() {
    this.form = this.formBuilder.group({
      firstName: [''],
      lastNamePrefix: [''],
      lastName: [''],
      email: ['', Validators.email]
    });
  }

  addEmployee() {
    console.log('add', this.form.getRawValue());
    const data = this.form.getRawValue();
    const index = this.userService.addEmployee({
      email: data.email,
      firstName: data.firstName,
      lastNamePrefix: data.lastNamePrefix,
      lastName: data.lastName,
    });

    this.router.navigate(['/admin/employees'], {queryParams: {user: index}});
  }
}
