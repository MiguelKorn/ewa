import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-employee-detail',
  templateUrl: './employee-detail.component.html',
  styleUrls: ['./employee-detail.component.css']
})
export class EmployeeDetailComponent implements OnInit {
  projects: any[];
  userId: number | null;
  breadcrumb: { title: string, route: string };

  constructor(private route: ActivatedRoute) {
    this.projects = [
      {
        title: 'Project 1',
        description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque interdum urna eu eros maximus iaculis. ' +
          'Phasellus lectus mauris, accumsan quis accumsan vel, ornare eget nulla.',
      },
      {
        title: 'Project 2',
        description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque interdum urna eu eros maximus iaculis. ' +
          'Phasellus lectus mauris, accumsan quis accumsan vel, ornare eget nulla.',
      },
      {
        title: 'Project 3',
        description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque interdum urna eu eros maximus iaculis. ' +
          'Phasellus lectus mauris, accumsan quis accumsan vel, ornare eget nulla.',
      },
    ];
  }

  ngOnInit() {
    this.route.paramMap.subscribe(params => {
      const userId = params.get('id');
      this.userId = userId === null ? null : Number(params.get('id'));
      this.breadcrumb = this.userId === null ? {title: 'Details', route: 'Account'} : {title: 'Werknemer details', route: 'Werknemers'};
    });
  }

}
