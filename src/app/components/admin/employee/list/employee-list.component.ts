import {Component, OnInit} from '@angular/core';
import {UserService} from '../../../../services/user.service';
import {NzMessageService} from 'ng-zorro-antd';
import {Router} from '@angular/router';

@Component({
  selector: 'app-employee-list',
  templateUrl: './employee-list.component.html',
  styleUrls: ['./employee-list.component.css']
})
export class EmployeeListComponent implements OnInit {
  constructor(private userService: UserService,
              private message: NzMessageService,
              private router: Router
  ) {
  }

  ngOnInit() {
  }

  public removeEmployee(id: number) {
    const deleted = this.userService.removeEmployee(id);
    this.userService.paginate();
    this.message.create('success', `${deleted.firstName} ${deleted.lastName} succesvol verwijderd!`);
  }
}
