import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';

@Component({
  selector: 'app-code',
  templateUrl: './code.component.html',
  styleUrls: ['./code.component.css']
})
export class CodeComponent implements OnInit {
  taal:string = "Nederlands";
  code:number;
  error: string;

  constructor( private router: Router ) { }

  ngOnInit() {
  }

  onEnterCode(){
    // check of code in database staat
    let codeString: string;
    let length :number = 0;
    try {
      codeString = this.code.toString();
      length= codeString.length;
    }catch (e) {
      length = 0;
    }

    if(length == 5 ){
      this.error = "";

      this.router.navigate(['intro']);
      // rout naar project pagina
    }else if(length < 5){
      this.error = "De code is te kort, probeer het opnieuw!";
    }
  }

}
