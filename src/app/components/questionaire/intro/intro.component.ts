import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';

@Component({
  selector: 'app-intro',
  templateUrl: './intro.component.html',
  styleUrls: ['./intro.component.css']
})
export class IntroComponent implements OnInit {
  title:string = "Oosterpark";
  info:string = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua." +
    " Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur." +
    " Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.";

  taal:string;
  leeftijd:number;
  geslacht:string;
  error: string;


  constructor( private router: Router ) { }

  ngOnInit() {
  }


  onStart(){
    if(this.leeftijd != null){
      if (this.leeftijd>120 || this.leeftijd<0){
        this.error = "Je leeftijd is niet correct";
      }else{
        this.error = "";

        //stuur leeftijd naar database
      }
    }
    //stuur geslacht naar database

    this.router.navigate(['questionaire']);
    console.log("Leeftijd en geslacht zijn naar de database gestuurd, ga naar begin van de questionair");

  }

}
