import { Component, OnInit } from '@angular/core';
import {QuestionaireService} from "../../../services/questionaire.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-outro',
  templateUrl: './outro.component.html',
  styleUrls: ['./outro.component.css']
})
export class OutroComponent implements OnInit {

  constructor( private questionaireService: QuestionaireService, private router: Router ) { }


  ngOnInit() {
  }

  goBack() {
    this.router.navigate(['questionaire']);
  }


  goToResults() {
    this.router.navigate(['result']);
  }
}
