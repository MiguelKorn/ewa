import { AfterViewInit, Component, ViewChild, ElementRef } from '@angular/core';
import { Chart } from 'chart.js';
import ChartLabels from 'chartjs-plugin-labels';
import {QuestionaireService} from '../../../services/questionaire.service';
import {Question} from '../../../models/question';
import {Router} from "@angular/router";

@Component({
  selector: 'app-result',
  templateUrl: './result.component.html',
  styleUrls: ['./result.component.css']
})
export class ResultComponent {
  private myPieChart: Chart;
  @ViewChild('canvas', {static: false}) canvas: ElementRef;
  private questionNames = ['Natuur beleven','Tuinieren','Rust & Ontspanning','Ontmoeting','Bewegen'];
  private matrix = [];

  private firstPlace = "";
  private secondPlace = "";
  private thirdPlace = "";




  private filledQuestions: Question[];

  constructor( private questionairService: QuestionaireService,  private router: Router ) {
   // this.getDataFromService();

  }

  ngAfterViewInit() {
    this.createChart();
    this.getDataFromService();
  }
  matrixInit(){
    const collumn = 5;
    const row = 5;
    for(var index =0; index<collumn; index++) {
      this.matrix[index] = new Array(row);
      this.matrix[index][index] = 1;
    }
    return this.matrix;
  }

  getDataFromService() {
    this.filledQuestions = this.questionairService.getQuestions();
    this.matrixInit();
    for (let index = 0; index < this.questionNames.length; index++) {
      for (let questionNumber = 0; questionNumber < this.filledQuestions.length; questionNumber++) {
        let firstSubject = this.filledQuestions[questionNumber].getFirstSubject();
        let secondSubject = this.filledQuestions[questionNumber].getSecondSubject();
        let answer = ResultComponent.convertAnswer(this.filledQuestions[questionNumber].getAnswerAsEnum());

        if (this.filledQuestions[questionNumber].getFirstSubject().getName() == this.questionNames[index]) {
          this.matrix[index][this.questionNames.indexOf(secondSubject.getName())] = answer;
        }
        else if (this.filledQuestions[questionNumber].getSecondSubject().getName() == this.questionNames[index]) {
          this.matrix[index][this.questionNames.indexOf(firstSubject.getName())] = answer;
        }
      }
    }
    let normalizedEigenVector = this.calculateScore();
    this.updateChart(normalizedEigenVector);
    //TODO fix top 3
    //this.visualiseTopThree(normalizedEigenVector);
  }

  static convertAnswer(answer){
    let convertedAnswer;
    switch (answer) {
      case 1:
          convertedAnswer = -8;
        break;
      case 2:
          convertedAnswer = -4;
        break;
      case 3:
          convertedAnswer = 1;
        break;
      case 4:
          convertedAnswer = 0.25;
        break;
      case 5:
          convertedAnswer = 0.125;
        break;
    }
    return convertedAnswer;
  }

  calculateScore(){
    /**
     * 1 wordt gedeeld door het linker deel van de matrix
     * dit moet volgens de formule.
     */
    let index = 0;
    for (let i = 0; i < this.matrix.length ; i++) {
      for (let j = 0; j < index ; j++) {
        this.matrix[i][j] =  (1 / this.matrix[i][j]);
      }
      index++;
    }

    /**
     * er wordt een totaal berekent van de rijen.
     */
    let sumOfMatrix = [];
    let totalCollumn;
    for (let i = 0; i < this.matrix.length ; i++) {
      totalCollumn = 0;
      for (let j = 0; j < this.matrix.length ; j++) {
            totalCollumn += this.matrix[j][i];
      }
      sumOfMatrix.push(totalCollumn);
    }

    /**
     * elke rij wordt gedeeld door het totaal van de rij.
     */
    for (let i = 0; i < this.matrix.length; i++) {
      for (let j = 0; j < this.matrix.length; j++) {
        this.matrix[j][i] = (this.matrix[j][i] / sumOfMatrix[i]);
      }
    }


    /**
     * de matrix rijen worden bij elkaar opgeteld
     * hier uit krijg je een percentage ook wel
     * de "normalized EigenVector" genoemd.
     */
    let normalizedEigenVector = [];
        let totalRow;
        for (let i = 0; i < this.matrix.length; i++) {
          totalRow = 0;
          for (let j = 0; j < this.matrix.length; j++) {
            totalRow += this.matrix[i][j];
          }

          normalizedEigenVector.push(totalRow / this.matrix.length);
        }

    let totaleWaarde = 0 ;
    for (let i = 0; i < normalizedEigenVector.length ; i++) {
      totaleWaarde += normalizedEigenVector[i];
    }
    console.log("het totaal: "+totaleWaarde);
    return normalizedEigenVector;
  }

  visualiseTopThree(normalizedEigenVector){
    console.log(normalizedEigenVector);
    var nameValueMap = new Map();
    for (let i = 0; i < normalizedEigenVector.length; i++) {
      nameValueMap.set(normalizedEigenVector[i], this.questionNames[i]);
    }

    for (let i = 0; i < normalizedEigenVector.length; i++) {
      //TODO create top 3
    }
  }

  updateChart(normalizedEigenVector){
    for(let index = 0; index < normalizedEigenVector.length; index++) {
      this.addToChart(this.questionNames[index], normalizedEigenVector[index]);
    }
  }

  addToChart(label, data ){
    this.myPieChart.data.labels.push(label);
    this.myPieChart.data.datasets.forEach((dataset) => {
      dataset.data.push(data);
    });
    this.myPieChart.update();
  }

  goBack(){
    this.router.navigate(['outro']);
  }

  createChart(){
    this.myPieChart = new Chart(this.canvas.nativeElement, {
      type: 'pie',
			data: {
				datasets: [{
					data: [],
					backgroundColor: [
						'#9B59B6',
            '#AF7AC5',
            '#C39BD3',
            '#D7BDE2',
            '#D2B4DE',
					],
				}],
				labels: []
			},
      plugins: [ChartLabels],
			options: {
        plugins: {
          labels: {
            render: 'label',
            position: 'default',
            fontColor: '#fff',
            overlap: true
          }
        },
        legend: {
          display: false,
        },
        tooltips: {
         enabled: true,
       },
				responsive: true,
      }
			});
    }
}

