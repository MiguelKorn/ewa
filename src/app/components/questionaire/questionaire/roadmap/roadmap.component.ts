import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {QuestionaireService} from '../../../../services/questionaire.service';
import {Question} from '../../../../models/question';
import {Router} from '@angular/router';

@Component({
  selector: 'app-roadmap',
  templateUrl: './roadmap.component.html',
  styleUrls: ['./roadmap.component.css']
})

export class RoadmapComponent implements OnInit {
  private currentQuestionIndex: number;

  @Output() onQuestionNavigate = new EventEmitter<Question>();

  constructor( private questionaireService: QuestionaireService, private router: Router ) { }

  ngOnInit(): void {
    this.currentQuestionIndex = 0;
    this.onQuestionNavigate.emit(this.questionaireService.getQuestionByIndex(this.currentQuestionIndex));
  }

  public navigateLeft() {
    if (this.hasPreviousQuestion()) {
      this.currentQuestionIndex--;
      this.onQuestionNavigate.emit(this.questionaireService.getQuestionByIndex(this.currentQuestionIndex));
    }
  }

  public navigateRight() {
    if (this.hasNextQuestion()) {
      this.currentQuestionIndex++;
      this.onQuestionNavigate.emit(this.questionaireService.getQuestionByIndex(this.currentQuestionIndex));
    } else {
      this.router.navigate(['outro']);
    }
  }

  /**
   * Check if there is a previous question.
   */
  public hasPreviousQuestion(): boolean {
    return (this.currentQuestionIndex > 0);
  }

  /**
   * Check if there is a next question.
   */
  public hasNextQuestion(): boolean {
    return (this.currentQuestionIndex < (this.questionaireService.getNumberOfQuestions() - 1));
  }
}
