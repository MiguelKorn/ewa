import {Component, EventEmitter, Input, Output} from '@angular/core';
import {Question} from '../../../../models/question';
import {QuestionEnum} from '../../../../models/question.enum';

@Component({
  selector: 'app-emotionbar',
  templateUrl: './emotionbar.component.html',
  styleUrls: ['./emotionbar.component.css']
})
export class EmotionbarComponent {
  @Input() question: Question;
  @Output() onSliderChange: EventEmitter<QuestionEnum> = new EventEmitter<QuestionEnum>();

  public changeValue( answer: QuestionEnum ) {
    this.question.answer = answer;
    this.onSliderChange.emit(answer);
  }
}
