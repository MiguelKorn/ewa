import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmotionbarComponent } from './emotionbar.component';

describe('EmotionbarComponent', () => {
  let component: EmotionbarComponent;
  let fixture: ComponentFixture<EmotionbarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmotionbarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmotionbarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
