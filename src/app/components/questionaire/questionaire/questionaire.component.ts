import {Component, ElementRef, ViewChild} from '@angular/core';
import {QuestionaireService} from '../../../services/questionaire.service';
import {Question} from '../../../models/question';
import {QuestionEnum} from '../../../models/question.enum';

@Component({
  selector:    'app-questionaire',
  templateUrl: './questionaire.component.html',
  styleUrls:   ['./questionaire.component.css']
})
export class QuestionaireComponent {
  private currentQuestion: Question;
  @ViewChild('rightsubject', {static: true}) rightsubject: ElementRef;
  @ViewChild('leftsubject', {static: true}) leftsubject: ElementRef;


  constructor( private questionaireService: QuestionaireService ) { }

  updateCurrentQuestion( question: Question ) {
    this.resetAnimations();
    this.currentQuestion = question;
    this.animatePreference(this.currentQuestion.getAnswerAsEnum());
  }

  resetAnimations() {
    this.leftsubject.nativeElement.classList.remove('expanded');
    this.leftsubject.nativeElement.classList.remove('folded');
    this.leftsubject.nativeElement.classList.remove('blurred');
    this.rightsubject.nativeElement.classList.remove('blurred');
  }

  animatePreference( answer: QuestionEnum ) {
    this.resetAnimations();

    switch (answer) {
      case QuestionEnum.PrefersSubject1:
        this.leftsubject.nativeElement.classList.add('expanded');
        this.rightsubject.nativeElement.classList.add('blurred');

        break;
      case QuestionEnum.SlightlyPrefersSubject1:
        this.leftsubject.nativeElement.classList.add('expanded');
        break;
      case QuestionEnum.SlightlyPrefersSubject2:
        this.leftsubject.nativeElement.classList.add('folded');
        break;
      case QuestionEnum.PrefersSubject2:
        this.leftsubject.nativeElement.classList.add('folded');
        this.leftsubject.nativeElement.classList.add('blurred');
        break;
    }
  }
}
