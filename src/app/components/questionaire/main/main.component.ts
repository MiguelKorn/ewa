import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {Router} from "@angular/router";
import {LanguageServiceService} from "../../../services/language-service.service";

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent implements OnInit {
   taal:string = "Nederlands";


  constructor(private router: Router , private languageSevice: LanguageServiceService) {

  }

  ngOnInit() {
  }

  onChangeLanguage(){
    this.languageSevice.setTaal = this.taal;
    console.log(this.languageSevice.getTaal)

  }

}
