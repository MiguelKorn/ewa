import {Component, Input, OnInit} from '@angular/core';
import {LanguageServiceService} from "../../../services/language-service.service";

@Component({
  selector: 'app-generalinfo',
  templateUrl: './generalinfo.component.html',
  styleUrls: ['./generalinfo.component.css']
})
export class GeneralinfoComponent implements OnInit {
  info: string ="De GGD Amsterdam is de grootste en oudste Geneeskundige en Gezondheidsdienst van Nederland en heeft een breed scala aan werkzaamheden op het terrein van de volksgezondheid.\n" +
    "      Vanaf de opsplitsing van GGD Amstelland de Meerlanden per 1 januari 2008 heeft de GGD Amsterdam een werkgebied dat de gemeenten Amsterdam, Amstelveen, Uithoorn (plaatsen Uithoorn en De Kwakel),\n" +
    "      Ouder-Amstel (plaatsen Duivendrecht en Ouderkerk a/d Amstel), Diemen, Aalsmeer (plaatsen Aalsmeer en Kudelstaart) bestrijkt.\n" +
    "      Gezamenlijk betreft dit ca. een miljoen inwoners.\n" +
    "      Ongeveer 1300 GGD medewerkers, verspreid over meer dan dertig gebouwen in de regio, zijn iedere dag druk in de weer om de gezondheid van de inwoners te bewaken en te bevorderen.\n" +
    "      Hier onder kun je jouw code invullen en nadat je op enter hebt gedrukt word je doorverwezen naar het project in jouw buurt.\n" +
    "      Vervolgens kun je jouw mening/voorkeur geven op bepaalde onderwerpen";


  constructor(private languageService: LanguageServiceService) {
    languageService.onLanguageChange.subscribe( taal =>{
      
        this.info = "The GGD Amsterdam is the largest and oldest Medical and Health Service in the Netherlands and has a wide range of activities in the field of public health. From the division of GGD Amstelland de Meerlanden on 1 January 2008, the GGD Amsterdam has a working area that the municipalities of Amsterdam, Amstelveen, Uithoorn (places Uithoorn and De Kwakel),Ouder-Amstel (places Duivendrecht and Ouderkerk a / d Amstel), Diemen, Aalsmeer (places Aalsmeer and Kudelstaart) covers.Together this concerns approximately one million inhabitants.So 1300 GGD employees, spread over more than thirty buildings in the region, are busy every day to monitor and discover the health of the residents.Below you can enter your code and after pressing enter you will be referred to the project in your area.You can then give your opinion / preference on certain topics"

    });
  }

  ngOnInit() {
  }


}
