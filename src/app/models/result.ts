import {Question} from './question';

export class Result {
  id: number;
  age: number | null; // optional
  gender: 'm' | 'v' | null; // optional
  zipcode: number | null; // optional, only numbers
  email: string | null; // optional
  projectId: number;
  score: number | null;
  questions: Question[];

  constructor(id: number,
              age: number | null,
              gender: 'm' | 'v' | null,
              zipcode: number | null,
              email: string | null,
              projectId: number,
              score: number | null,
              questions: Question[]) {
    this.id = id;
    this.age = age;
    this.gender = gender;
    this.zipcode = zipcode;
    this.email = email;
    this.projectId = projectId;
    this.score = score;
    this.questions = questions;
  }
}
