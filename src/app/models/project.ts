export class Project {
  private _id: string;
  private _name: string;
  private _description: string;
  private _area: number;
  private _neighbourhood: number;
  private _district: number;
  private _position: {
    latitude: number;
    longitude: number;
  };
  private _type: string;
  private _surfaceArea: number;
  private _endDate: Date;


  constructor(id: string,
              name: string,
              description: string,
              area: number,
              neighbourhood: number,
              district: number,
              position: { latitude: number; longitude: number },
              type: string,
              surfaceArea: number,
              endDate: Date) {
    this._id = id; /*
    should be district code + totalNumber of project that uses this code + 1:
    e.g if no code = A00e-1, if 3project with code = A00e-1
    */
    this._name = name;
    this._description = description;
    this._area = area;
    this._neighbourhood = neighbourhood;
    this._district = district;
    this._position = position;
    this._type = type;
    this._surfaceArea = surfaceArea;
    this._endDate = endDate;
  }

  get id(): string {
    return this._id;
  }

  get name(): string {
    return this._name;
  }

  set name(value: string) {
    this._name = value;
  }

  get description(): string {
    return this._description;
  }

  set description(value: string) {
    this._description = value;
  }

  get area(): number {
    return this._area;
  }

  set area(value: number) {
    this._area = value;
  }

  get neighbourhood(): number {
    return this._neighbourhood;
  }

  set neighbourhood(value: number) {
    this._neighbourhood = value;
  }

  get district(): number {
    return this._district;
  }

  set district(value: number) {
    this._district = value;
  }

  get position(): { latitude: number; longitude: number } {
    return this._position;
  }

  set position(value: { latitude: number; longitude: number }) {
    this._position = value;
  }

  get type(): string {
    return this._type;
  }

  set type(value: string) {
    this._type = value;
  }

  get surfaceArea(): number {
    return this._surfaceArea;
  }

  set surfaceArea(value: number) {
    this._surfaceArea = value;
  }

  get endDate(): Date {
    return this._endDate;
  }

  set endDate(value: Date) {
    this._endDate = value;
  }
}
