import {QuestionEnum} from './question.enum';
import {Subject} from './subject';

export class Question {
  subject1: Subject;
  subject2: Subject;
  answer: QuestionEnum;

  /**
   * Create new Question.
   *
   * @param subject1 Subject
   * @param subject2 Subject
   * @param answer   QuestionEnum
   */
  public constructor(
    subject1: Subject,
    subject2: Subject,
    answer ?: QuestionEnum
  ) {
    this.subject1 = subject1;
    this.subject2 = subject2;
    this.answer = (answer != null) ? answer : QuestionEnum.Neutral;
  }

  /**
   * Get first subject.
   */
  public getFirstSubject(): Subject {
    return this.subject1;
  }

  /**
   * Get second subject.
   */
  public getSecondSubject(): Subject {
    return this.subject2;
  }

  /**
   * Get answer as enum;
   */
  public getAnswerAsEnum(): QuestionEnum {
    return this.answer;
  }
}
