export enum QuestionEnum
{
  PrefersSubject1 = 1,
  SlightlyPrefersSubject1 = 2,
  Neutral = 3,
  SlightlyPrefersSubject2 = 4,
  PrefersSubject2 = 5
}
