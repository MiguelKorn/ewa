export class Subject
{
  name: string;
  imageLocation: string;
  info: string;

  /**
   * Construct new Subject.
   *
   * @param name           string
   * @param imageLocation  string
   * @param info           string
   */
  public constructor(
   name: string,
   imageLocation: string,
   info: string
  ) {
    this.name = name;
    this.imageLocation = imageLocation;
    this.info = info;
  }

  /**
   * Get name.
   */
  public getName(): string
  {
    return this.name;
  }

  /**
   * Get image location.
   */
  public getImageLocation(): string
  {
    return this.imageLocation;
  }

  /**
   * Get more information about subject.
   */
  public getInfo(): string
  {
    return this.info;
  }
}
