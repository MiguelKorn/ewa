import {Project} from './project';

export class User {
  id: number;
  username: string;
  password: string;
  email: string;
  firstName: string;
  lastNamePrefix: string | null;
  lastName: string;
  avatar: string;
  gender: string;
  projects: Project[];
  numProjects = Math.ceil(Math.random() * 10);
  resetPassword: boolean;
  fillCredentials: boolean;
  phonePrefix: string;
  phoneNumber: number;
  function: string;
  area: string;

  // constructor(username: string, password: string) {
  //   this._username = username;
  //   this._password = password;
  // }

  constructor(email: string,
              firstName: string,
              lastNamePrefix: string | null,
              lastName: string,
              data: {
                id: number,
                username: string,
                password: string,
                avatar: string
              } | null) {
    this.email = email;
    this.firstName = firstName;
    this.lastNamePrefix = lastNamePrefix;
    this.lastName = lastName;
    this.projects = [];
    this.fillCredentials = true;

    if (data !== null) {
      if (data.id !== undefined && data.username !== undefined && data.password !== undefined && data.avatar !== undefined) {
        this.fillCredentials = false;
      }

      if (data.id !== undefined) {
        this.id = data.id;
      }
      if (data.username !== undefined) {
        this.username = data.username;
      }

      // TODO: Generate random pwd in BE
      if (data.password !== undefined) {
        this.password = data.password;
        this.resetPassword = false;
      } else {
        this.password = 'welcomeUser';
        this.resetPassword = true;
      }

      if (data.avatar !== undefined) {
        this.avatar = data.avatar;
      }
    }
  }

  public getFullname(): string {
    return `${this.firstName} ${this.lastNamePrefix !== null ? this.lastNamePrefix + ' ' : ''}${this.lastName}`;
  }

  public addProject(project: Project): number {
    return this.projects.push(project) - 1;
  }
}
